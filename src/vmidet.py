# -*- coding: utf-8 -*-

import argparse
import os
import subprocess
import configparser
from enum import Enum

VOLATILITY_PROFILE = ''
VOLATILITY_LOCATION = ''
VOLATILITY_COMMAND = ''


class Plugins(Enum):
    """
    Enum für die genutzten Volatility-Plugins
    """
    LINUX_PSLIST = 'linux_pslist'
    LINUX_PSAUX = 'linux_psaux'
    LINUX_NETSTAT = 'linux_netstat'
    LINUX_LSMOD = 'linux_lsmod'
    LINUX_HIDDEN_MODULES = 'linux_hidden_modules'
    LINUX_PSTREE = 'linux_pstree'
    LINUX_CHECK_MODULES = 'linux_check_modules'
    LINUX_LSOF = 'linux_lsof'


def parse_arguments():
    """
    Liest die beim Start des Scripts übergebenen Parameter ein

    :return:
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('machine', help='Name der Gast-VM, die monitored werden soll.\Format: one-xxx')
    parser.add_argument('-s', '--store', help='Ausgabedatei, wird erstellt, wenn nicht vorhanden.',
                        default='output.txt')
    parser.add_argument('-d', '--diff', help='Pfad zur Vergleichsdatei')
    args = parser.parse_args()

    if args.store:
        if args.store == args.diff:
            print('store und diff sind gleich')

    return args.machine, args.store, args.diff


def read_config():
    """
    Liest aus der config.ini die Werte für das Volatility-Kommando, den Speicherort des images sowie das zu nutzende
    Profil aus.

    :return:
    """
    global VOLATILITY_LOCATION, VOLATILITY_PROFILE, VOLATILITY_COMMAND
    section = 'VOLATILITY'  # das ist die Standard-Section in der Config-Datei

    config = configparser.ConfigParser()
    config.read('config.ini')

    # das hier ist nur für Entwicklungszwecke, wenn eine Datei 'dev' existiert, dann lesen wir aus der DEV-Section
    if os.path.exists('dev'):
        section = 'VOLATILITY_DEV'

    try:  # jetzt setzen wir die Werte global
        VOLATILITY_LOCATION = config.get(section, 'location')
        VOLATILITY_PROFILE = config.get(section, 'profile')
        VOLATILITY_COMMAND = config.get(section, 'command')
    except configparser.NoSectionError as n:
        print('Section "[{}]" fehlt in config.ini.\nProgramm wird beendet.'.format(n.section))
        exit(0)
    except configparser.NoOptionError as o:
        print('Eintrag "{}" fehlt in Section "[{}]" in config.ini.\nProgramm wird beendet.'.format(o.option, o.section))
        exit(0)


def mount_vmifs(machine):
    """
    Mounten des vmifs für die angegebene Maschine. Zuerst mal unmounten, falls noch etwas gemounted ist

    :param machine: Maschinenname der guest-vm im Format one-xxx

    :return: None
    """
    command_unmount = 'umount /mnt'
    command_mount = 'vmifs name {} /mnt'.format(machine)
    p = subprocess.Popen(command_unmount, shell=True)
    p.communicate()

    p = subprocess.Popen(command_mount, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    stdout, stderr = p.communicate()
    if stderr.decode('UTF-8').find('VMI_ERROR') > -1:
        print(stderr.decode('UTF-8'))
        print('Programm wird beendet')
        exit(0)


def read_image(store_file):
    """
    Liest laufende Prozesse, Netzwerkverbindungen, Kernel-Module und Hidden-Modules aus dem RAM.
    Wenn beim Aufruf des Scripts die Option --store gesetzt war, dann werden die Infos in die Datei mit dem übergebenen
    Dateinamen geschrieben

    :param store_file: Datei, in die die Infos gespeichert werden sollen

    :return: Die Ausgaben der Volatility-Aufrufe *linux_pslist*, *linux_psaux*, *linux_pstree*, *linux_nestat*,
    *linux_lsmod* und *linux_hidden_modules*
    """
    pslist_header_text = '***** pslist *****\n'
    psaux_header_text = '***** psaux *****\n'
    pstree_header_text = '***** pstree *****\n'
    lsof_header_text = '***** lsof *****\n'
    netstat_header_text = '***** netstat *****\n'
    lsmod_header_text = '***** lsmod *****\n'
    check_modules_header_text = '***** check_modules *****\n'
    hidden_modules_header_text = '***** hidden_modules *****\n'

    print('[+] linux_pslist')
    pslist = volatility_cmd(Plugins.LINUX_PSLIST.value)
    print('[+] linux_psaux')
    psaux = volatility_cmd(Plugins.LINUX_PSAUX.value)
    print('[+] linux_pstree')
    pstree = volatility_cmd(Plugins.LINUX_PSTREE.value)
    print('[+] linux_lsof')
    lsof = volatility_cmd(Plugins.LINUX_LSOF.value)
    print('[+] linux_netstat')
    netstat = volatility_cmd(Plugins.LINUX_NETSTAT.value)
    print('[+] linux_lsmod')
    lsmod = volatility_cmd(Plugins.LINUX_LSMOD.value)
    print('[+] linux_check_modules')
    check_modules = volatility_cmd(Plugins.LINUX_CHECK_MODULES.value)
    print('[+] linux_hidden_modules\n')
    hidden_modules = volatility_cmd(Plugins.LINUX_HIDDEN_MODULES.value)

    if store_file:  # wenn gespeichert werden soll, dann ins File schreiben (alter Inhalt wird überschrieben)
        os.makedirs(os.path.dirname(os.path.abspath(store_file)), exist_ok=True)  # Ordnerpfad erzeugen
        with open(store_file, 'w') as outfile:
            outfile.write(pslist_header_text)
            outfile.write(pslist)
            outfile.write('\n')

            outfile.write(psaux_header_text)
            outfile.write(psaux)
            outfile.write('\n')

            outfile.write(pstree_header_text)
            outfile.write(pstree)
            outfile.write('\n')

            outfile.write(lsof_header_text)
            outfile.write(lsof)
            outfile.write('\n')

            outfile.write(netstat_header_text)
            outfile.write(netstat)
            outfile.write('\n')

            outfile.write(lsmod_header_text)
            outfile.write(lsmod)
            outfile.write('\n')

            outfile.write(check_modules_header_text)
            outfile.write(check_modules)
            outfile.write('\n')

            outfile.write(hidden_modules_header_text)
            outfile.write(hidden_modules)

        print('[+] Ausgabe gespeichert nach "{}"'.format(store_file))

    return pslist, psaux, lsof, netstat, lsmod, check_modules, hidden_modules


def volatility_cmd(plugin):
    """
    Führt das Volatility-Commando für das übergebene Plugin aus. Die globalen Variablen werden vorher aus der config.ini
    gelesen und bedeuten Folgendes:

    * VOLATILITY_COMMAND - das vollständige Kommando für Volatility
    * VOLATILITY_LOCATION - Pfad zum image
    * VOLATILITY_PROFILE - Name des Profils

    Die Ausführung des Kommandos geschieht über das Python-Modul subprocess

    :param plugin: Name des zu verwendenden Plugins

    :return: None
    """
    command = '{} -f {} --profile {} {}'.format(VOLATILITY_COMMAND, VOLATILITY_LOCATION, VOLATILITY_PROFILE, plugin)

    p = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    stdout, stderr = p.communicate()
    return stdout.decode('utf-8')  # Ausgabe ist in Bytes, deshalb noch nach UTF-8 decodieren


def compare_infos(pslist, psaux, lsof, netstat, lsmod, check_modules, hidden_modules, diff_file):
    """
    Hier werden die ausgelesenen Infos mit denen der übergebenen Datei verglichen. Dazu wird der Datentyp *set*
    verwendet, der die Bildung von Differenzmengen ermöglicht. So lassen sich die einzelnen Einträge sehr gut
    vergleichen.

    :param pslist: Ausgabe von *linux_pslist*
    :param psaux: Ausgabe von *linux_psaux*
    :param lsof: Ausgabe von *linux_lsof*
    :param netstat: Ausgabe von *linux_netstat*
    :param lsmod: Ausgabe von *linux_lsmod*
    :param check_modules: Ausgabe von *linux_check_modules*
    :param hidden_modules: Ausgabe von *linux_hidden_modules*
    :param diff_file: Datei, in der die älteren Infos stehen

    :return: None
    """
    # Zuerst verpacken wir alle Infos in sets, der einfachere Fall sind die gerade ausgelesenen Infos
    set_pslist_new = set()
    set_psaux_new = set()
    set_lsof_new = set()
    set_netstat_new = set()
    set_lsmod_new = set()
    set_check_modules_new = set()
    set_hidden_modules_new = set()

    # die sets für die Infos aus der diff-Datei
    set_pslist_diff = set()
    set_psaux_diff = set()
    set_lsof_diff = set()
    set_netstat_diff = set()
    set_lsmod_diff = set()
    set_check_modules_diff = set()
    set_hidden_modules_diff = set()

    # zuerst die Prozessliste, hier müssen die ersten zwei Zeilen weg, da das die Überschrift mit Trennlinie ist
    for line in pslist.split('\n'):
        if line == '':
            continue
        if line[0:6] == 'Offset':  # erste Zeile
            continue
        if line[0:18].find('--') > -1:  # zweite Zeile
            continue

        # dann fügen wir jede Zeile einzeln dem Set hinzu (das strip entfernt evtl. Whitespaces am Ende und Anfang)
        set_pslist_new.add(line.strip())

    # dann psaux
    for line in psaux.split('\n'):
        if line == '':
            continue
        elif line[0:3] == 'Pid':  # erste Zeile überspringen
            continue
        # dann fügen wir jede Zeile einzeln dem Set hinzu (das strip entfernt evtl. Whitespaces am Ende und Anfang)
        set_psaux_new.add(line.strip())

    # dann lsof
    for line in lsof.split('\n'):
        if line == '':
            continue
        elif line[0:6] == 'Offset':  # erste Zeile
            continue
        elif line[0:18].find('--') > -1:  # zweite Zeile
            continue
        set_lsof_new.add(line.strip())

    # dann netstat
    for line in netstat.split('\n'):
        if line[0:4] == 'UNIX':  # die RAW-Sockets lassen wir hier mal außer Acht
            continue
        if not line == '':
            set_netstat_new.add(line.strip())

    # dann lsmod
    for line in lsmod.split('\n'):
        if not line == '':
            set_lsmod_new.add(line.strip())

    # dann check_modules
    for line in check_modules.split('\n'):
        if line == '':
            continue
        elif line[0:18].find('Module Address'):  # erste Zeile
            continue
        elif line[0:18].find('--') > -1:  # zweite Zeile
            continue
        else:
            set_check_modules_new.add(line.strip())

    # und hidden_modules
    for line in hidden_modules.split('\n'):
        if line == '':
            continue
        if line[0:6] == 'Offset':  # erste Zeile
            continue
        if line[0:18].find('--') > -1:  # zweite Zeile
            continue
        set_hidden_modules_new.add(line.strip())

    '''Jetzt aus dem diff-file die Infos rausholen. Das diff-file ist wie folgt aufgebaut: 
    * Überschrift beginnend mit 5 Sternen beinhaltend "pslist", "psaux", "netstat", "lsmod", "hidden_modules"
    * danach folgen die Ausgaben des jeweiligen Volatility-Plugins
    * nach jeder fertigen Ausgabe eine Leerzeile. pstree wird übersprungen, da Vergleiche hier nicht sinnvoll sind.
    Hier ist es nur gut, den passenden Tree zu haben um zu sehen, wessen Elternprozess ein verdächtiger Prozess ist'''

    section = -1  # hiermit halten wir beim Einlesen des diff-Files fest, an welcher Stelle wir uns befinden

    with open(diff_file, 'r') as diff:
        lines = diff.readlines()
        for line in lines:
            if line == '':  # Leerzeile überspringen
                continue
            if line.find('pslist') > -1:
                section = Plugins.LINUX_PSLIST
                continue
            elif line.find('psaux') > -1:
                section = Plugins.LINUX_PSAUX
                continue
            elif line.find('lsof') > -1:
                section = Plugins.LINUX_LSOF
                continue
            elif line.find('netstat') > -1:
                section = Plugins.LINUX_NETSTAT
                continue
            elif line.find('lsmod') > -1:
                section = Plugins.LINUX_LSMOD
                continue
            elif line.find('check_modules') > -1:
                section = Plugins.LINUX_CHECK_MODULES
                continue
            elif line.find('hidden_modules') > -1:
                section = Plugins.LINUX_HIDDEN_MODULES
                continue
            elif line.find('pstree') > -1:
                section = Plugins.LINUX_PSTREE

            # jetzt für jede Section der Textdatei die Werte einlesen und in das passende Set speichern
            if section == Plugins.LINUX_PSLIST:
                if line[0:6] == 'Offset':  # erste Zeile unwichtig
                    continue
                elif line[0:18].find('--') > -1:  # zweite Zeile beinhaltet am Anfang ein paar Minuse, unwichtig
                    continue
                else:
                    set_pslist_diff.add(line.strip())

            elif section == Plugins.LINUX_PSAUX:
                if line[0:3] == 'Pid':  # erste Zeile unwichtig
                    continue
                set_psaux_diff.add(line.strip())

            elif section == Plugins.LINUX_NETSTAT:
                if line[0:4] == 'UNIX':  # die RAW-Sockets lassen wir hier mal außer Acht
                    continue
                else:
                    set_netstat_diff.add(line.strip())

            elif section == Plugins.LINUX_LSOF:
                if line[0:6] == 'Offset':  # erste Zeile unwichtig
                    continue
                elif line[0:18].find('--') > -1:  # zweite Zeile beinhaltet am Anfang ein paar Minuse, unwichtig
                    continue
                else:
                    set_lsof_diff.add(line.strip())

            elif section == Plugins.LINUX_LSMOD:  # hier sind alle Zeilen ok
                set_lsmod_diff.add(line.strip())

            elif section == Plugins.LINUX_CHECK_MODULES:
                if line[0:18].find('Module Address'):  # erste Zeile
                    continue
                elif line[0:18].find('--') > -1:  # zweite Zeile
                    continue
                else:
                    set_check_modules_diff.add(line.strip())

            elif section == Plugins.LINUX_HIDDEN_MODULES:
                if line[0:6] == 'Offset':  # erste Zeile unwichtig
                    continue
                elif line[0:18].find('--') > -1:  # zweite Zeile beinhaltet am Anfang ein paar Minuse, unwichtig
                    continue
                else:
                    set_hidden_modules_diff.add(line.strip())

            else:  # für alle nicht zu vergeleichenden sections (pstree)
                continue

    # hier werden jetzt die Differenzmengen gebildet, das ist in Python ziemlich einfach
    set_pslist_new -= set_pslist_diff
    set_psaux_new -= set_psaux_diff
    set_lsof_new -= set_lsof_diff
    set_netstat_new -= set_netstat_diff
    set_lsmod_new -= set_lsmod_diff
    set_check_modules_new -= set_check_modules_diff
    set_hidden_modules_new -= set_hidden_modules_diff

    # und noch die Ausgabe
    print('\n[+] New pslist-items compared to diff:')
    print('--------------------------------------')
    for item in set_pslist_new:
        print(item)

    print('\n[+] New psaux-items compared to diff:')
    print('-------------------------------------')
    for item in set_psaux_new:
        print(item)

    print('\n[+] New lsof-items compared to diff:')
    print('-------------------------------------')
    for item in set_lsof_new:
        print(item)

    print('\n[+] New network connections compared to diff:')
    print('---------------------------------------------')
    for item in set_netstat_new:
        print(item)

    print('\n[+] New kernel modules compared to diff:')
    print('----------------------------------------')
    for item in set_lsmod_new:
        print(item)

    print('\n[+] New modules found bey check_modules compared to diff:')
    print('---------------------------------------------------------')
    for item in set_check_modules_new:
        print(item)

    print('\n[+] New hidden modules compared to diff:')
    print('----------------------------------------')
    for item in set_hidden_modules_new:
        print(item)


def main():
    machine, store, diff = parse_arguments()  # zuerst alle Argumente einlesen
    read_config()

    if not os.path.exists('dev'):  # vmifs wird in developement-Version nicht gemounted
        mount_vmifs(machine)

    print('[*] Durchführen der relevanten Volatility-Plugins\n')
    pslist, psaux, lsof, netstat, kernel_modules, check_modules, hidden_modules = read_image(store)

    if diff:  # und wenn diff gesetzt ist, dann noch die Unterschiede ermitteln
        print('[*] Vergleichen der Ergebnisse mit "{}".'.format(diff))
        if not os.path.exists(diff):
            print('[-] Die Datei "{}" existiert nicht, Programm wird beendet.'.format(diff))
            exit(0)
        compare_infos(pslist, psaux, lsof, netstat, kernel_modules, check_modules, hidden_modules, diff)

    print('[*] ***** ENDE *****')


if __name__ == '__main__':
    main()
