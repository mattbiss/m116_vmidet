# Beschreibung des Programms vmidet.py

Bitte die vollständige Beschreibung lesen, bevor das Programm ausgeführt wird.

## Voraussetzungen

* Python3 installiert

## Programmstart


Das Programm wird gestartet mit folgenden Parametern:

* machine - Name der monitored guest-vm, Format one-xxxxx
* [--store, -s] - optional, gibt den Dateinamen an, in den die Ausgabe gespeichert werden soll
* [--diff, -d]  - optional, gibt Namen eines vorher gespeicherten Outfiles an, mit dem die aktuellen Ergebnisse verglichen werden

Wird das Programm ohne Parameter gestartet, wird als default-output die Datei "output.txt" angelegt.

Beispiel:

        vmidet.py one-xxxxx --store out.txt
        vmidet.py one-xxxxx --store out2.txt --diff out.txt
        vmidet.py one-xxxxx --diff out.txt 

## Konfigurationsdatei config.ini

In der Konfigurationsdatei *config.ini* müssen drei Optionen in der Sektion [VOLATILITY] eingetragen werden:

* command
* profile
* location

Die config.ini muss sich im selben Ordner wie das Python-Script befinden.

###### command
Hier muss der vollständige Pfad zum Ausführen von volatility eingetragen sein. Da es in einigen Fällen (bspw. via ssh) zu
Problemen kommen kann, wenn nur "volatility" als Kommando angegeben wird, sollte hier aus Kompatibilitätsgründen folgender 
Eintrag gemacht werden: `python2.7 /Pfad/zur/vol.py`
        
        
Im Fall der csec-Maschinen der genutzten OpenNebula-Umgebung sieht dieser Eintrag wie folgt aus:

        command=python2.7 /usr/src/volatility/vol.py
        
###### profile
Hier wird das zum Speicherdump passende Profil angegeben. Im Fall dieser Hausarbeit lautet dieses: **LinuxDebian8x64**

###### location
Hier muss der Ort des Images eingetragen werden. Im Fall der Hausarbeit lautet dies: **/mnt/mem** 


Eine für den Anwendungsfall der Hausarbeit korrekte config.ini sieht demnach wie folgt aus:
        
        [VOLATILITY]
        command=python2.7 /usr/src/volatility/vol.py
        profile=LinuxDebian8x64
        location=/mnt/mem


## Funktionsweise 
Vorab: die einzelnen Funktionsbeschreibungen sind direkt im Quellcode ausführlich zu finden.

Bei Programmstart werden die übergebenen Parameter eingelesen. Danach wird zuerst das vmifs mit dem übergebenen Namen 
gemounted. Im Anschluss daran werden aus dem Image, welches in der *config.ini* als location eingetragen wurde, folgende
Infos via volatility-plugins ausgelesen:

* linux_pslist
* linux_psaux
* linux_pstree
* linux_lsof
* linux_netstat
* linux_lsmod
* linux_check_modules
* linux_hidden_modules

Diese Informationen werden nacheinander in die mit --store angegebene Datei (oder default output.txt) geschrieben. Dabei
erhält jedes Ergebnis eine Überschriftenzeile nach dem Format `***** <plugin> *****` 

Zum Ausführen der Volatility-Plugins wird das Python-Modul *subprocess* genutzt, welches den Aufruf anderer Programme
in einer eigenen Shell ermöglicht (im Quellcode Methode `volatility_command(plugin)`). 

War der Parameter --diff beim Scriptaufruf nicht gesetzt, ist das Programm hier fertig.

### Erweiterung mit --diff (-d)

Wird ein File zum Vergleich übergeben (dieses muss zuvor mit --store erzeugt worden sein), werden die Inhalte dieses Files
mit den aktuellen Werten verglichen. Dies erfolgt in der Methode *compare_infos()*.

Hierzu wird wie im vorherigen Abschnitt aus den angegebenen Plugins der Zustand ausgelesen (Ausnahme: pstree wird nicht compared, 
da hier der gesamte Prozessbaum für die Analyse interessant ist). Die Ausgaben werden nun allerdings Zeilenweise einem *set*
zugeordnet. Der Datentyp *set* in Python entspricht mathematisch einer Menge. Daher ist auf es diesen Datentypen möglich, 
Mengenoperationen auszuführen. 

Zusätzlich wird nun aus den Angaben der Plugins im übergebenen diff-File ebenfalls zu jedem Plugin ein Set der einzelnen
Einträge erzeugt (deshalb werden die Überschriften in der Ausgabedatei erzeugt).

Nun kann von den jeweils zusammengehörigen Sets die Differenzmenge gebildet werden (in Python durch eine Subtraktion dargestellt)
und es bleiben nur die Veränderungen gegenüber dem diff-File über. Diese werden noch leserlich auf der Konsole ausgegeben.

Exemplarisch ist dies hier für *netstat* dargestellt:

        def compare_infos(pslist, psaux, netstat, lsmod, hidden_modules, diff_file):
            set_netstat_new = set()
            set_netstat_diff = set()
            
            for line in netstat.split('\n'):
                set_netstat_new.add(line.strip())
                
            section = -1  # hiermit halten wir beim Einlesen des diff-Files fest, an welcher Stelle wir uns befinden

            with open(diff_file, 'r') as diff:
                lines = diff.readlines()
                .
                .
                if line.find('netstat') > -1:
                    section = Plugins.LINUX_NETSTAT
                    continue
                .
                .    
                if section == Plugins.LINUX_NETSTAT:
                    if line[0:4] == 'UNIX':  # die RAW-Sockets lassen wir hier mal außer Acht
                        continue
                    else:
                        set_netstat_diff.add(line.strip())
                        
            set_netstat_new -= set_netstat_diff  # in set_netstat_new stehen jetzt die Unterschiede
            
## Test-Script test_vmidet

Script muss mit **sudo** ausgeführt werden. 

Dieses Testscript kann zum Überprüfen der Ausführung des gesamten Ablaufs genutzt werden. Hierbei muss dem Script der Name der zu testenden
Maschine (one-xxx) übergeben werden. 

Nun werden folgende Schritte durchgeführt:

1. ein Durchlauf von `vmidet.py one-xxxxx --store before`
2. Durchführung des Angriffs
3. 10 Durchläufe von `vmidet one-xxxxx --store out/$file_name_out --diff before`. $file_name_out wird dabei "out_durchlauf" genannt
und in den Unterordner out verschoben. Die Ausgabe von vmidet.py wird in den Unterordner "during" verschoben und erhält
als Dateiname "during_uhrzeit". Im Unterordner "during" finden sich danach also 10 Dateien, die als Vergeleiche zum Zustand
vor dem Angriff dienen.
            
            
Beispiel:

        sudo ./test_vmidet one-46542

